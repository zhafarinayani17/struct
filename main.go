package main

import "fmt"

type User struct {
	ID        int
	FristName string
	LastName  string
	Email     string
	IsActive  bool
}

func (user User) display() string {
	return fmt.Sprintf("Name : %s %s, Emai : %s", user.FristName, user.LastName, user.Email)
}

func displayUser(user User) string {
	return fmt.Sprintf("Name : %s %s, Emai : %s", user.FristName, user.LastName, user.Email)
}

type Gruop struct {
	Name        string
	admin       User
	users       []User
	IsAvailable bool
}

func (group Gruop) displayGroup() {
	fmt.Printf("Name : %s", group.Name)
	fmt.Println("")
	fmt.Printf("Member count : %d", len(group.users))
	fmt.Println("")
	fmt.Println("User Name:")
	for _, user := range group.users {
		fmt.Println(user.FristName)
	}
}

func main() {
	user := User{1, "Alyani", "Zhafarina", "zhafarinayani17@gmail.com", true}

	result := user.display()

	fmt.Println(result)
	users := []User{user}

	group := Gruop{"HRD", user, users, true}

	group.displayGroup()
	// displayGroup(group)

}

func displayGroup(group Gruop) {
	fmt.Printf("Name : %s", group.Name)
	fmt.Println("")
	fmt.Printf("Member count : %d", len(group.users))
	fmt.Println("")
	fmt.Println("User Name:")
	for _, user := range group.users {
		fmt.Println(user.FristName)
	}
}
